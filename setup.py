#!/usr/bin/env python
from distutils.core import setup

from setuptools import find_packages


setup(**{
    'name': 'router',
    'version': '0.0.1',
    'author': 'Wojciech Zelek',
    'author_email': 'zelo@zelo.pl',
    'py_modules': ['router'],
    'requires': [],
    'extras_require': {
        'dev': ['pytest>=5'],
     },
})
