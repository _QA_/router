# TODO: add matching/generation for scheme, subdomains and base path
# TODO: add some error handling with useful error messages
# TODO: add docs

from typing import Optional
import re
from string import Formatter
from urllib.parse import urlsplit, urlunsplit, urlencode, parse_qs


class Router:
    def __init__(self):
        self.registry = {}

    def add(
        self, name: str, path_template: str, fragment_template: Optional[str] = None
    ):
        self.registry[name] = Route(
            name=name, path_template=path_template, fragment_template=fragment_template
        )

    def generate(self, name, query=None, fragment=None, **variables):
        return self.registry[name].generate(query=query, fragment=fragment, **variables)

    def match(self, url):
        for route in self.registry.values():
            match = route.match(url)
            if match:
                return match


def parse_template(pattern):
    variable_names = set()
    format_string = r""
    regexp_string = r""
    for literal_text, field_name, format_spec, _conversion in Formatter().parse(
        pattern
    ):
        format_string += literal_text
        regexp_string += literal_text
        if field_name:
            if not format_spec:
                raise ValueError(
                    "Each capturing group must have proper regexp assigned."
                )
            format_string += "{" + field_name + "}"
            regexp_string += "(?P<" + field_name + ">" + format_spec + ")"
            variable_names.add(field_name)
    format_string = format_string.lstrip("^").rstrip("$")
    return {
        "variables": variable_names,
        "template": format_string,
        "regexp": re.compile(regexp_string),
    }


def parse_query(query):
    query = parse_qs(qs=query, keep_blank_values=True)
    query = {k: (v[0] if len(v) == 1 else v) for k, v in query.items()}
    return query


class Route:
    def __init__(
        self, name: str, path_template: str, fragment_template: Optional[str] = None
    ):
        self.name = name
        self._path = parse_template(path_template)
        self._fragment = (
            parse_template(fragment_template) if fragment_template else None
        )

    def generate(self, query=None, fragment=None, **variables):
        path = self._path["template"].format(**variables)
        if self._fragment:
            fragment = fragment or self._fragment["template"].format(**variables)
        path = urlunsplit(
            ("", "", path, urlencode(query or "", doseq=True), fragment or "")
        )
        return path

    def match(self, url):
        _, _, path, query, fragment = urlsplit(url)

        if not (path_match := self._path["regexp"].match(path)):
            return None
        else:
            variables = path_match.groupdict()

        if self._fragment:
            if not (fragment_match := self._fragment["regexp"].match(fragment)):
                return None
            else:
                variables.update(fragment_match.groupdict())
        return {
            "name": self.name,
            "variables": variables,
            "query": parse_query(query),
            "fragment": fragment or None,
        }
