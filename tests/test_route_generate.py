import pytest

from router import Route


@pytest.mark.parametrize('path_template, fragment_template,variables,query,fragment,exp_path', [
    (r'', None, {}, None, None, ''),
    (r'/', None, {}, None, None, '/'),
    (r'/one', None, {}, None, None, '/one'),
    (r'/one/two', None, {}, None, None, '/one/two'),
    (r'/one', r'fragment', {}, None, None, '/one#fragment'),
    (r'/one/two', r'frag/ment', {}, None, None, '/one/two#frag/ment'),
    (r'/one/two', r'frag/ment/{three:]d+}', {'three': 4}, None, None, '/one/two#frag/ment/4'),
    (r'/one/two', r'frag/ment/{three:]d+}', {'three': 4}, None, 'override/fragment', '/one/two#override/fragment'),
    (r'/{one:[a-z]{3}}/two', None, {'one': 'ONE'}, None, None, '/ONE/two'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}', None, {'one': 'ONE', 'three': 3}, None, None, '/ONE/two/3'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', None, {'one': 'ONE', 'three': 3}, None, None, '/ONE/two/3/four/'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', None, {'one': 'ONE', 'three': 3}, None, 'anchor', '/ONE/two/3/four/#anchor'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', None, {'one': 'ONE', 'three': 3}, {'a': 'b'}, None, '/ONE/two/3/four/?a=b'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', None, {'one': 'ONE', 'three': 3}, {'a': 'b', 'b': 1}, None, '/ONE/two/3/four/?a=b&b=1'),
    (r'/{one:[a-z]{3}}/two/{three:]d+}/four/', None, {'one': 'ONE', 'three': 3}, {'a': ['b', 'b'], 'b': 1}, None, '/ONE/two/3/four/?a=b&a=b&b=1'),
])
def test_route_generate(path_template, fragment_template, variables, query, fragment, exp_path):
    route = Route(name='test:name', path_template=path_template, fragment_template=fragment_template)
    path = route.generate(query=query, fragment=fragment, **variables)
    assert path == exp_path
