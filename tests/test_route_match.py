import pytest

from router import Route


@pytest.mark.parametrize('name, path_template, fragment_template, url, exp_name, exp_variables, exp_query, exp_fragment', [
    ('test:name', r'^/one/2/three/4$', None, 'http://example.com/one/2/three/4/', None, {}, {}, None),
    ('test:name', r'^/one/2/three/4$', None, 'http://example.com/one/2/three/4', 'test:name', {}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, '/one/2/three/4', 'test:name', {'two': '2', 'four': '4'}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, 'http://example.com/one/2/three/4', 'test:name', {'two': '2', 'four': '4'}, {}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, 'http://example.com/one/2/three/4?a=a&b=b', 'test:name', {'two': '2', 'four': '4'}, {'a': 'a', 'b': 'b'}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, 'http://example.com/one/2/three/4?a=a&a=b', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b']}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, 'http://example.com/one/2/three/4?a=a&a=b&c=c', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b'], 'c': 'c'}, None),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', None, 'http://example.com/one/2/three/4?a=a&a=b&c=c#anchor', 'test:name', {'two': '2', 'four': '4'}, {'a': ['a', 'b'], 'c': 'c'}, 'anchor'),
    ('test:name', r'^/one/2/three/4$', r"^some/fragment$", 'http://example.com/one/2/three/4/#some/fragment', None, {}, {}, 'some/fragment'),
    ('test:name', r'^/one/2/three/4$', r"^some/other/fragment$", 'http://example.com/one/2/three/4#some/other/fragment', 'test:name', {}, {}, 'some/other/fragment'),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', r'^fragment/{five:\d+}$', '/one/2/three/4#fragment/6', 'test:name', {'two': '2', 'four': '4', 'five': '6'}, {}, 'fragment/6'),
    ('test:name', r'^/one/{two:\d}/three/{four:\w+}$', r'^fragment/{five:[\w\-]+}$', 'http://example.com/one/2/three/4#fragment/abc-123_ABC', 'test:name', {'two': '2', 'four': '4', 'five': 'abc-123_ABC'}, {}, 'fragment/abc-123_ABC'),
])
def test_route_match(name, path_template, fragment_template, url, exp_name, exp_variables, exp_query, exp_fragment):
    route = Route(name=name, path_template=path_template, fragment_template=fragment_template)
    match = route.match(url)
    if exp_name is None:
        assert match is None
    else:
        assert match == {
            'name': exp_name,
            'variables': exp_variables,
            'query': exp_query,
            'fragment': exp_fragment,
        }
